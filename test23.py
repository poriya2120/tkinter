from tkinter import *
from tkinter import ttk
root=Tk()
count=0
def counter(lbl):
    count=0
    def counts():
        global count
        count+=1
        lbl.config(text=str(count))
        lbl.after(1000,counts)
    counts()
root.geometry("500x500")
root.title("test22")
lbl=ttk.Label(root,background="red")
lbl.pack()
counter(lbl)
btn=ttk.Button(root,text="exit",width=30,
               command=root.destroy)
btn.pack()

root.mainloop()
