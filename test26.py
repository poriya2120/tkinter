from tkinter import *
from tkinter import ttk
root=Tk()
root.geometry("500x500")
root.title("test26")
notebook=ttk.Notebook(root)
notebook.pack()

frame1=ttk.Frame(notebook)
frame2=ttk.Frame(notebook)
frame3=ttk.Frame(notebook)
notebook.add(frame1,text="tab1",state='hidden')
notebook.add(frame2,text="tab2",state='normal')
notebook.add(frame3,text="tab3",state='disable')
ttk.Button(frame1,text="clickme").pack()
ttk.Scale(frame2).pack()

root.mainloop()
