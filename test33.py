from tkinter import *
from tkinter import Menu

root=Tk()
root.title('test33')
def message():
    print("hello menu")
root.geometry("500x500")

menubar=Menu(root)

#create file menu
filemenu=Menu(menubar)

filemenu.add_command(label="Open",command=message)
filemenu.add_command(label="Save")
filemenu.add_separator()
filemenu.add_command(label="Exit")

menubar.add_cascade(label="File",menu=filemenu)

#create help menu

helpmenu=Menu(menubar)
helpmenu.add_command(label="About")
helpmenu.add_command(label="helpme")

menubar.add_cascade(label="Help",menu=helpmenu)

root.config(menu=menubar)
root.mainloop()
