from tkinter import *
from tkinter import ttk
from random import randint
root=Tk()
root.title("test34")
root.geometry("400x400")
w=Canvas(root,width=300,height=300)
w.pack()
w.create_line(20,15 ,300 ,100)
w.create_line(0,300,300,0,fil="red",dash=(4,4))

w.create_polygon(50,40,70,90,130,150)

w.create_rectangle(20,25,150,80,fill="blue")

r=20
for i in range (10):
    x=randint(0+r,300-r)
    y=randint(0+r,300-r)
    w.create_oval(x-r,y-r,x+r,y+r,fil="purple")
root.mainloop()
