from tkinter import *
from tkinter import ttk
root=Tk()
root.title("test20")
root.geometry("500x500")
#progress=ttk.Progressbar(root,orient=VERTICAL,length=200).pack()
progress=ttk.Progressbar(root,orient=HORIZONTAL,length=200)
progress.pack()
progress.config(mode='indeterminate')
progress.start()
progress.stop()
progress.config(mode='determinate',maximum=11.0,value=6.0)
value=DoubleVar()
progress.config(variable=value)
scale=ttk.Scale(root,orient=HORIZONTAL,length=400,variable=value,
                from_=0.0,to=11.0)
scale.pack()
scale.set(3.0)
root.mainloop()
